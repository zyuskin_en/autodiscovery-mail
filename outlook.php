<?php
//http://virer.net/info/ol-autodiscover/index.html
//get raw POST data so we can extract the email address
$data = @file_get_contents("php://input");
preg_match("/\<EMailAddress\>(.*?)\<\/EMailAddress\>/", $data, $matches);
//$matches[1] = 'ezyuskin@dagc.ru';
$email = (isset($matches[1]) && trim($matches[1]) != '' && filter_var(trim($matches[1]), FILTER_VALIDATE_EMAIL)) ? trim($matches[1]) : die();
list ($user_name, $domain) = explode('@', $email);

$config_file = realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'mailservers' . DIRECTORY_SEPARATOR.strtolower($domain) . '.php';

if (!file_exists($config_file))
	die();

$config = include $config_file;

//set Content-Type
header("Content-Type: application/xml");
?>
<?php echo '<?xml version="1.0" encoding="utf-8" ?>'; ?>
<Autodiscover xmlns="http://schemas.microsoft.com/exchange/autodiscover/responseschema/2006">
<Response xmlns="http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a">
<Account>
<AccountType>email</AccountType>
<Action>settings</Action>
<?php if (isset($config['protocols']['pop'])): ?>
<Protocol>
<Type>POP3</Type>
<Server><?=$config['protocols']['pop']['server']?></Server>
<Port><?=$config['protocols']['pop']['port']?></Port>
<LoginName><?=($config['protocols']['pop']['logon_name_as_email']) ? $email : $user_name; ?></LoginName>
<DomainRequired><?=($config['protocols']['pop']['domain_required']) ? 'on' : 'off'; ?></DomainRequired>
<SPA><?=($config['protocols']['pop']['spa']) ? 'on' : 'off'; ?></SPA>
<SSL><?=($config['protocols']['pop']['ssl']) ? 'on' : 'off'; ?></SSL>
<AuthRequired><?=($config['protocols']['pop']['auth_required']) ? 'on' : 'off'; ?></AuthRequired>
</Protocol>
<?php endif;?>
<?php if (isset($config['protocols']['imap'])): ?>
<Protocol>
<Type>IMAP</Type>
<Server><?=$config['protocols']['imap']['server']?></Server>
<Port><?=$config['protocols']['imap']['port']?></Port>
<LoginName><?=($config['protocols']['imap']['logon_name_as_email']) ? $email : $user_name; ?></LoginName>
<DomainRequired><?=($config['protocols']['imap']['domain_required']) ? 'on' : 'off'; ?></DomainRequired>
<SPA><?=($config['protocols']['imap']['spa']) ? 'on' : 'off'; ?></SPA>
<SSL><?=($config['protocols']['imap']['ssl']) ? 'on' : 'off'; ?></SSL>
<AuthRequired><?=($config['protocols']['imap']['auth_required']) ? 'on' : 'off'; ?></AuthRequired>
</Protocol>
<?php endif;?>
<?php if (isset($config['protocols']['smtp'])): ?>
<Protocol>
<Type>SMTP</Type>
<Server><?=$config['protocols']['smtp']['server']?></Server>
<Port><?=$config['protocols']['smtp']['port']?></Port>
<LoginName><?=($config['protocols']['smtp']['logon_name_as_email']) ? $email : $user_name; ?></LoginName>
<DomainRequired><?=($config['protocols']['smtp']['domain_required']) ? 'on' : 'off'; ?></DomainRequired>
<SPA><?=($config['protocols']['smtp']['spa']) ? 'on' : 'off'; ?></SPA>
<SSL><?=($config['protocols']['smtp']['ssl']) ? 'on' : 'off'; ?></SSL>
<AuthRequired><?=($config['protocols']['smtp']['auth_required']) ? 'on' : 'off'; ?></AuthRequired>
<UsePOPAuth><?=($config['protocols']['smtp']['use_pop_auth']) ? 'on' : 'off'; ?></UsePOPAuth>
<SMTPLast><?=($config['protocols']['smtp']['smtp_last']) ? 'on' : 'off'; ?></SMTPLast>
</Protocol>
<?php endif;?>
</Account>
</Response>
</Autodiscover>
